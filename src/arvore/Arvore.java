package arvore;


/*
 *@author Gustavo Carvalho, Gustavo Felipe, João Hitallo, Michel Victor
 *
 */

import java.io.*;
import java.util.*;

public class Arvore {
	private Node raiz; // primeiro nó da arvore

	// ==============================================================
	// O CONSTRUTOR
	// ==============================================================	

	// Construtor
	public Arvore() {
		raiz = null;
	}
	
	// ==============================================================
	// BUSCA UM ELEMENTO NA ARVORE
	// ==============================================================	

	// Encontra o nó por um valor buscado
	public Node buscar(int key) {
		// assume-se que a árvore não esteja vazia
		Node atual = raiz; // inicia o atual como raiz

		// enquanto não encontrar nada
		while (atual.dado != key) {
			if (key < atual.dado) // se o valor for menor que o atual, vai pra esquerda
				atual = atual.esquerda;
			else // se não, vai pra direita
				atual = atual.direita;
			if (atual == null) // caso não encontre retorna null
				return null;
		}
		return atual; // retorna o valor
	}

	// ==============================================================
	// INSERE UM NOVO ELEMENTO NA ARVORE
	// ==============================================================	

	public void inserir(int dado) {
		Node newNode = new Node(); // cria novo nó
		newNode.dado = dado; // insere o dado no nó

		// caso não haja nenhum nó, ele cria uma nova raiz
		if (raiz == null)
			raiz = newNode;
		// caso haja uma raiz, continua a inserção
		else {
			Node atual = raiz; // inicia como raiz
			Node pai;
			// (exits internally)
			while (true) {
				pai = atual;

				// verifica se o dado que se deseja inserir, é menor que o dado atual
				// caso seja menor, será inserido na esquerda
				if (dado < atual.dado) {
					atual = atual.esquerda;
					// caso seja o fim da linha
					if (atual == null) {
						// insere na esquerda
						pai.esquerda = newNode;
						return;
					}
				}
				// caso o que se queira inserir seja maior
				else {
					atual = atual.direita;
					// if end of the line
					if (atual == null) { // insert on right
						pai.direita = newNode;
						return;
					}
				}
			}
		}
	}

	// ==============================================================
	// BUSCA E APAGA UM VALOR DA ARVORE, CASO EXISTA
	// ==============================================================	

	// deleta um nó através de um valor
	public boolean delete(int key) {
		// assume-se que a arvore não está vazia
		Node atual = raiz;
		Node pai = raiz;
		boolean isFilhoEsquerdo = true;

		// procura pelo nó
		while (atual.dado != key) {
			pai = atual;
			if (key < atual.dado) {
				// se o valor buscado for menor que o do nó atual, ele vai para a esquerda
				isFilhoEsquerdo = true;
				atual = atual.esquerda;
			} else {
				// caso seja maior, vai para a direita
				isFilhoEsquerdo = false;
				atual = atual.direita;
			}
			if (atual == null) // caso não encontre
				return false;
		}

		// Encontrado o nó que será deletado

		// se for uma folha, simplesmente apaga
		if (atual.esquerda == null && atual.direita == null) {
			if (atual == raiz) // se o atual for uma raiz apaga a raiz
				raiz = null;
			else if (isFilhoEsquerdo)
				pai.esquerda = null; // desconecta
			else // caso seja filho direito
				pai.direita = null;
		} else if (atual.direita == null) {
			// se não houver filho direito, substitui com subárvore esquerda
			if (atual == raiz)
				raiz = atual.esquerda;
			else if (isFilhoEsquerdo)
				pai.esquerda = atual.esquerda;
			else
				pai.direita = atual.esquerda;
		} else if (atual.esquerda == null) {
			// se não houver filho esquerdo, substitui com subárvore direita
			if (atual == raiz)
				raiz = atual.direita;
			else if (isFilhoEsquerdo)
				pai.esquerda = atual.direita;
			else
				pai.direita = atual.direita;
		} else {
			// dois filhos, então substitui com o sucessor inOrder
			// pega o sucessor do nó para deletar
			Node successor = getSucessor(atual);

			// conecta o parente ao sucessor atual instanciado
			if (atual == raiz)
				raiz = successor;
			else if (isFilhoEsquerdo)
				pai.esquerda = successor;
			else
				pai.direita = successor;

			// conecta o sucessor atual ao seu filho direito
			successor.esquerda = atual.esquerda;
		}
		return true;
	}

	// ==============================================================
	// APAGA TODOS OS VALORES PARES DA ARVORE
	// ==============================================================	

	public void deleteAllPares() {
		boolean isFilhoEsquerdo = true;

		ArrayList<Integer> elementosArvore = elementosArvore();

		System.out.println(this.totalNodes());
		for (int i = 0; i < elementosArvore.size(); i++) {
			Node atual = raiz;
			Node pai = raiz;
			if (elementosArvore.get(i) % 2 == 0) {
				
				while ( atual.dado != elementosArvore.get(i)) {
					pai = atual;
					if (elementosArvore.get(i) < atual.dado) {
						isFilhoEsquerdo = true;
						atual = atual.esquerda;
					} else {
						isFilhoEsquerdo = false;
						atual = atual.direita;
					} 
					if(atual == null){
						break;
					}
				}
				
				if (atual.esquerda == null && atual.direita == null) {
					if (atual == raiz) // caso seja a raiz, apaga o valor dela
						raiz = null;
					else if (isFilhoEsquerdo)
						pai.esquerda = null;
					else
						pai.direita = null;
				}
				
				else if (atual.direita == null) {
					if (atual == raiz)
						raiz = atual.esquerda;
					else if (isFilhoEsquerdo)
						pai.esquerda = atual.esquerda;
					else
						pai.direita = atual.esquerda;
				}
				
				else if (atual.esquerda == null) {
					if (atual == raiz)
						raiz = atual.direita;
					else if (isFilhoEsquerdo)
						pai.esquerda = atual.direita;
					else
						pai.direita = atual.direita;
				}
				
				else {
					// dois filhos, então substitui com o sucessor inOrder
					// pega o sucessor do nó para deletar
					Node successor = getSucessor(atual);

					// conecta o parente ao sucessor atual instanciado
					if (atual == raiz)
						raiz = successor;
					else if (atual.dado < pai.dado)
						pai.esquerda = successor;
					else
						pai.direita = successor;

					// conecta o sucessor atual ao seu filho direito
					successor.esquerda = atual.esquerda;
				}
			}
		}
	}

	// ==============================================================
	// RETORNA O NÓ COM O PROXIMO MAIOR VALOR DEPOIS DE DELNODE
	// VAI PARA O FILHO CERTO, E ENTÃO PARA OS SEUS DESCENTENTES
	// DA SUA ESQUERDA
	// ==============================================================	

	private Node getSucessor(Node delNode) {
		Node paiSucesor = delNode;
		Node sucesor = delNode;
		Node atual = delNode.direita; // vai para o filho da direita
		// enquanto houver proximo
		while (atual != null) {
			// filho esquerdo
			paiSucesor = sucesor;
			sucesor = atual;
			atual = atual.esquerda; // vai para o filho da esquerda
		}
		// se o sucessor não for o filho da direita, faz as conecções
		if (sucesor != delNode.direita) {
			paiSucesor.esquerda = sucesor.direita;
			sucesor.direita = delNode.direita;
		}
		return sucesor;
	}

	// ==============================================================
	// ATRAVESSA A ARVORE BINARIA
	// ==============================================================	

	public String traverse(int traverseType) {
		String travessia = "";
		switch (traverseType) {
		case 1:
			System.out.print("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
			travessia += "\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n";
			System.out.print("Atravessamento em Pré-Ordem: ");
			travessia += "Atravessamento em Pré-Ordem: ";
			preOrder(raiz);
			travessia += "[" + preOrderString(raiz) + "]";
			System.out.print("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
			travessia += "\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n";
			break;
		case 2:
			System.out.print("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
			travessia += "\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n";
			System.out.print("Atravessamento em Ordem: ");
			travessia += "Atravessamento em Ordem: ";
			inOrder(raiz);
			travessia += "[" + inOrderString(raiz) + "]";
			System.out.print("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
			travessia += "\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n";
			break;
		case 3:
			System.out.print("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
			travessia += "\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n";
			System.out.print("Atravessamento em Pós-Ordem: ");
			travessia += "Atravessamento em Pós-Ordem: ";
			postOrder(raiz);
			travessia += "[" + postOrderString(raiz) + "]";
			System.out.print("\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
			travessia += "\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n";
			break;
		}
		System.out.println();
		return travessia;
	}

	// ==============================================================
	// METODOS PARA PERCORRER ARVORE
	// ==============================================================	
	
	
	private void preOrder(Node localRoot) {
		if (localRoot != null) {
			System.out.print(localRoot.dado + " ");
			preOrder(localRoot.esquerda);
			preOrder(localRoot.direita);
		}
	}
	
	private void inOrder(Node localRoot) {
		if (localRoot != null) {
			inOrder(localRoot.esquerda);
			System.out.print(localRoot.dado + " ");
			inOrder(localRoot.direita);
		}
	}
	
	private void postOrder(Node localRoot) {
		if (localRoot != null) {
			postOrder(localRoot.esquerda);
			postOrder(localRoot.direita);
			System.out.print(localRoot.dado + " ");
		}
	}

	// ==============================================================
	// EXIBE A ARVORE NA TELA
	// ==============================================================		

	public String mostrarArvore() {
		String arvore = "";
		
		Stack globalStack = new Stack();
		globalStack.push(raiz);
		int nBlanks = 32;
		boolean isRowEmpty = false;
		System.out.println("......................................................");
		arvore += "===================================\n";
		while (isRowEmpty == false) {
			Stack localStack = new Stack();
			isRowEmpty = true;

			for (int j = 0; j < nBlanks; j++) {
				System.out.print(' ');
				arvore += ' ';
			}
			
//			arvore += "\n"; 

			while (globalStack.isEmpty() == false) {
				Node temp = (Node) globalStack.pop();
				if (temp != null) {
					System.out.print(temp.dado);
					
					arvore += temp.dado;
					
					localStack.push(temp.esquerda);
					localStack.push(temp.direita);

					if (temp.esquerda != null || temp.direita != null)
						isRowEmpty = false;
				} else {
					System.out.print("--");
					arvore += "--";
					localStack.push(null);
					localStack.push(null);
				}
				for (int j = 0; j < nBlanks * 2 - 2; j++) {
					System.out.print(' ');
					arvore += ' ';
				}
			}
			System.out.println();
			arvore += "\n";
			nBlanks /= 2;
			while (localStack.isEmpty() == false)
				globalStack.push(localStack.pop());
		}
		System.out.println("......................................................");
		arvore += "===================================";
		return arvore;
	} 


	// ==============================================================
	// CALCULA A QUANTIDADE DE NÓS DA ARVORE
	// ==============================================================	
	
	// conta os nós da arvore a partir de determinado ponto
	public int contarNos(Node atual) {
		if (atual == null)
			return 0;
		else
			return (1 + contarNos(atual.esquerda) + contarNos(atual.direita));
	}

	// retorna o valor total de nós da árvore a partir da raiz
	public int totalNodes() {
		return contarNos(raiz);
	}

	// ==============================================================
	// CALCULA A QUANTIDADE DE FOLHAS DA ARVORE
	// ==============================================================	
	
	// conta a quantidade de folhas em uma arvore a partir de determinado nó
	public int contarFolhas(Node atual) {
		if (atual == null)
			return 0;
		if (atual.esquerda == null && atual.direita == null)
			return 1;
		return contarFolhas(atual.esquerda) + contarFolhas(atual.direita);
	}

	// retorna a quantidade total de folhas de uma árvore
	public int quantFolhas() {
		return contarFolhas(raiz);
	}

	// ==============================================================
	// RETORNA A QUANTIDADE DE NÓS NÃO-FOLHA DA ÁRVORE
	// ==============================================================	
	
	// Retorna a quantidade de nós não-folha de uma árvore
	public int quantNosNaoFolha() {
		return totalNodes() - quantFolhas();
	}

	// ==============================================================
	// CALCULA A ALTURA DA ÁRVORE
	// ==============================================================	
	
	// calcula a altura da arvore a partir de certo nó
	public int calculaAltura(Node atual) {
		if (atual == null)
			return -1; // altura da árvore vazia
		else {
			int he = calculaAltura(atual.esquerda);
			int hd = calculaAltura(atual.direita);
			if (he < hd)
				return hd + 1;
			else
				return he + 1;
		}
	}

	// retorna a altura total da árvore
	public int retornaAltura() {
		return calculaAltura(raiz);
	}

	// ==============================================================
	// RETORNA UMA STRING COM TODOS OS ELEMENTOS DA ÁRVORE
	// ==============================================================	
	
	private String preOrderString(Node localRoot) {
		String concat = "";
		if (localRoot != null) {
			concat += " " + localRoot.dado;
			System.out.print(localRoot.dado + " ");
			concat += preOrderString(localRoot.esquerda);
			concat += preOrderString(localRoot.direita);
		}
		return concat;
	}
	
	private String inOrderString(Node localRoot) {
		String concat = "";
		if (localRoot != null) {
			concat += inOrderString(localRoot.esquerda);
			System.out.print(" " + localRoot.dado);
			concat += " " + localRoot.dado;
			concat += inOrderString(localRoot.direita);
		}
		return concat + "";
	}
		
	private String postOrderString(Node localRoot) {
		String concat = "";
		if (localRoot != null) {
			concat += postOrderString(localRoot.esquerda);
			concat += postOrderString(localRoot.direita);
			System.out.print(localRoot.dado + " ");
			concat += " " + localRoot.dado;
		}
		return concat + "";
	}

	// ==============================================================
	// RETORNA UMA ARRAYLIST COM TODOS OS ELEMENTOS DA ÁRVORE
	// ==============================================================	

	public ArrayList<Integer> elementosArvore() {
		String concatenado = "";
		concatenado += inOrderString(raiz);
		String[] concatAux = concatenado.split(" ");
		
		ArrayList<Integer> elems = new ArrayList<>();

		for (int i = 0; i < concatAux.length; i++) {
			if(!concatAux[i].isEmpty()) {
				elems.add(Integer.parseInt(concatAux[i]));
			}
		}
		return elems;
	}
	
	// ==============================================================
	// RETORNA A RAIZ DA ARVORE
	// ==============================================================	

	public Node getRaiz() {
		return this.raiz;
	}
}
