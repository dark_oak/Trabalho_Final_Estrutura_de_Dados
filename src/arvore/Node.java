package arvore;

public class Node {
	public int dado; // dado do nó
	public Node esquerda; // this node's left child
	public Node direita; // this node's right child

	public Node() {
		//Default
	}
	
	public Node(int iData) {
		this.dado = iData; 
	}
	
	public void displayNode() // mostra o valor do nó
	{
		System.out.print('{');
		System.out.print(dado);
		System.out.print("} ");
	}
} // end class Node
