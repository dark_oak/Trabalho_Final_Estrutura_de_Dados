package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import arvore.*;

import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JRadioButton;

public class Principal extends JFrame {

	private JPanel contentPane;
	private Arvore yggdrasil = new Arvore();
	private JTextField inserido;
	private JTextField removido;

	/**
	 * Launch the application.
	 */
	public static void init() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 720, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(6, 0, 714, 443);
		contentPane.add(panel);
		panel.setLayout(null);

		JTextArea log = new JTextArea();
		log.setEditable(false);
		log.setLineWrap(true);
		log.setBounds(357, 6, 351, 431);
		log.setText(yggdrasil.mostrarArvore());
		panel.add(log);

		inserido = new JTextField();
		inserido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					yggdrasil.inserir(Integer.parseInt(inserido.getText()));
					log.setText(yggdrasil.mostrarArvore());
					inserido.setText("");
				}
			}
		});
		inserido.setFont(new Font("Dialog", Font.PLAIN, 14));
		inserido.setBounds(12, 15, 124, 25);
		panel.add(inserido);
		inserido.setColumns(10);

		JButton btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				yggdrasil.inserir(Integer.parseInt(inserido.getText()));
				inserido.setText("");
				log.setText(yggdrasil.mostrarArvore());
			}
		});
		btnInserir.setBounds(158, 15, 124, 25);
		panel.add(btnInserir);

		removido = new JTextField();
		removido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					yggdrasil.delete(Integer.parseInt(removido.getText()));
					log.setText(yggdrasil.mostrarArvore());
					removido.setText("");
				}
			}
		});
		removido.setFont(new Font("Dialog", Font.PLAIN, 14));
		removido.setBounds(12, 52, 124, 25);
		panel.add(removido);
		removido.setColumns(10);

		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				yggdrasil.delete(Integer.parseInt(removido.getText()));
				log.setText(yggdrasil.mostrarArvore());
				removido.setText("");
			}
		});
		btnRemover.setBounds(158, 52, 124, 25);
		panel.add(btnRemover);

		JButton btnMostrarInfo = new JButton("Mostrar Info");
		btnMostrarInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log.setText(yggdrasil.mostrarArvore() 
						+ "\n===================================\n"
						+ "Quantidade de Nós da Árvore: " + yggdrasil.totalNodes()
						+ "\nQuantidade de Folhas da Árvore: " + yggdrasil.quantFolhas()
						+ "\nQuantidade de Nós Não-Folha da Árvore: " + yggdrasil.quantNosNaoFolha()
						+ "\nAltura da árvore: " + yggdrasil.retornaAltura()
						+ "\n===================================\n");
			}
		});
		btnMostrarInfo.setBounds(158, 90, 124, 25);
		panel.add(btnMostrarInfo);

		JButton btnAtravessarPrO = new JButton("Atravessar Pré-Ordem");
		btnAtravessarPrO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log.setText(yggdrasil.mostrarArvore() + yggdrasil.traverse(1));
			}
		});
		btnAtravessarPrO.setBounds(89, 127, 193, 25);
		panel.add(btnAtravessarPrO);
		
		JButton btnNewButton = new JButton("Atravessar Ordem");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				log.setText(yggdrasil.mostrarArvore() + yggdrasil.traverse(2));
			}
		});
		btnNewButton.setBounds(89, 164, 193, 25);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Atravessar Pós-Ordem");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				log.setText(yggdrasil.mostrarArvore() + yggdrasil.traverse(3));
			}
		});
		btnNewButton_1.setBounds(89, 198, 193, 25);
		panel.add(btnNewButton_1);
	}
}
